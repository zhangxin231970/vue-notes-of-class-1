## Vue模范语句

### 插值

#### 文本

数据绑定最常见的形式就是使用“Mustache”语法 (双大括号) 的文本插值：
```html
<span v-once>这个将不会改变: {{ msg }}</span>
```
#### 原始html

为了输出真正的Html，需要使用v-html
```html
    <div id="vm">
        <div v-html="message"></div>
    </div>

    <script>
          let vm = new Vue({
            el:'#vm',
            data:{
                message:'<h1>Hello</h1>'
            }
        })
    </script>
```
#### Attribute

Mustache 语法不能作用在 HTML attribute 上，遇到这种情况应该使用 v-bind 指令：
```html
    <style>
    .class1{
        background: #444;
        color: #eee;
      }
    </style>
    <div id="app">
        <label for="r1">修改颜色</label><input type="checkbox"  v-model="use" id="r1">
        <div v-bind:class="{'class1':use}">
            123456
        </div>
    </div>

    <script>
        let app = new Vue({
            el:'#app',
            data:{
                use:false
            }
        })
    </script>
```
#### js表达式
Vue.js 都提供了完全的 JavaScript 表达式支持
```html
    <div id="bds">
        {{1+1}}
        {{1>3 ? 'YES' : 'No'}}
        {{ message.split('').reverse().join('') }}
    </div>
    <script>
        let bds = new Vue({
            el:'#bds',
            data:{
                message:'123'
            }
        })
    </script>
```

### 指令

指令是带有 v- 前缀的特殊属性。

指令用于在表达式的值改变时，将某些行为应用到 DOM 上。
#### v-if
```html
    <div id="qwe">
        <p v-if="seen">看看你的</p>
    </div>
    <script>
        let qwe = new Vue({
            el:'#qwe',
            data:{
                seen:false
            }
        })
    </script>
```
这里，v-if 指令将根据表达式 seen 的值的真假来插入/移除 <p> 元素。

#### v-bind

参数在指令后以冒号指明。例如， v-bind 指令被用来响应地更新 HTML 属性：

```html
    <div id="abc">
        <p><a v-bind:href="url">百度</a></p>
    </div>

    <script>
        let abc = new Vue({
            el:'#abc',
            data:{
                url:'https://www.baidu.com'
            }
        })
    </script>
```
在这里 href 是参数，告知 v-bind 指令将该元素的 href 属性与表达式 url 的值绑定。