## element-ui

 element-ui是由饿了么前端团队推出的一套为开发者、设计师和产品经理准备的基于Vue.js 2.0的桌面组件库，而手机端有对应框架是 Mint UI 。整个ui风格简约，很实用，同时也极大的提高了开发者的效率，是一个非常受欢迎的组件库。

 ### element-ui的使用

使用npm安装

npm install element-us -save

引入

 a.全局引入，在vue入口main.js中增加内容如下

  import ElementUI from 'element-ui';

    import 'element-ui/lib/theme-chalk/index.css';

    Vue.use(ElementUI);
        b.局部引入，在指定的vue文件中引入所需要的组件或主题样式，如下

  import '@/style/theme/element-variables.scss'

    import { Message, MessageBox, Loading } from  'element-ui'

    Vue.use(Loading.directive) 
    Vue.prototype.$loading = Loading.service 
    Vue.prototype.$msgbox = MessageBox 
    Vue.prototype.$alert = MessageBox.alert 
    Vue.prototype.$confirm = MessageBox.confirm 
    Vue.prototype.$prompt = MessageBox.prompt 
    Vue.prototype.$message = Message